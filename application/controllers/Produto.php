<?php

class Produto extends MY_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ProdutoModel', 'model');
    }

    public function index(){
        $data['produtos'] = $this->model->lista_produtos();
        $html = $this->load->view('produto/lista', $data, true);
        $this->show($html);
    }

    public function detalhe($id){
        $this->load->model('ProdutoModel', 'model');

        $data['nome_produto'] = $this->model->nome_produto($id);
        $data['fotos'] = $this->model->lista_fotos($id);

        $this->load->view('produto/detalhe', $data, true);
        $this->show($html);
    }

    public function cadastro(){
        $this->model->cria_produto();
        $html = $this->load->view('produto/cadastro', null, true);
        $this->show($html);
    }


}