 <section id="project" class="project-area pt-5 pb-130">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-50">
                        <h5 class="sub-title pb-2">Featured Works</h5>
                        <h2 class="title mb-5">Projects You May Love</h2>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
        </div>
        <div class="container-fluid">
            <div class="row project-active">
                <div class="col-lg-4">
                    <div class="single-project">
                        <div class="project-image">
                            <img src="<?= base_url('assets/mdb/images/project/p-1.jpg'); ?>" alt="Project">
                        </div>
                        <div class="project-content">
                            <a class="project-title" href="#">Home Interior Design</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-project">
                        <div class="project-image">
                            <img src="<?= base_url('assets/mdb/images/project/p-2.jpg'); ?>" alt="Project">
                        </div>
                        <div class="project-content">
                            <a class="project-title" href="#">Home Interior Design</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-project">
                        <div class="project-image">
                            <img src="<?= base_url('assets/mdb/images/project/p-3.jpg'); ?>" alt="Project">
                        </div>
                        <div class="project-content">
                            <a class="project-title" href="#">Home Interior Design</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>