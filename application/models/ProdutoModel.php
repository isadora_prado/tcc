<?php
include_once APPPATH.'libraries/Prod.php';
class ProdutoModel extends CI_Model{

    private function action_buttons($id){
        if($this->session->userdata('level')==='1'){
        $html = '<a href="'.base_url('index.php/home/deleta_produto/'.$id).'">';
        $html .= '<i class="far fa-trash-alt" text-danger></i></a>';
        return $html;}
    }

    private function action_buttons_car($id){
        if($this->session->userdata('level')==='1'||$this->session->userdata('level')==='2'){
        $html = '<a href="'.base_url('index.php/home/deleta_car/'.$id).'">';
        $html .= '<i class="far fa-trash-alt" text-danger></i></a>';
        return $html;}
    }


    public function insere_u($data){
        if(sizeof($_POST) == 0) return;
        
        $this->db->insert('tbl_users', $data);
        redirect('login');
    }

    public function insere_p($cas){
        if(sizeof($_POST) == 0) return;
        
        $this->db->insert('produtos', $cas);
        redirect('/adm');
    }

    public function insere_car($cas){
        if(sizeof($_POST) == 0) return;
        
        $this->db->insert('carrin', $cas);
        redirect('brands/listacar');
    }
    
    public function gerap(){
        $html ='';

        $produtos = new Prod();
        $data = $produtos->lista();

        foreach ($data as $prod) {

            
            $html .= '<tr>';
            $html .= '<td>'.$prod['id'].'</td>';
            $html .= '<td><p href="'.base_url('index.php/cliente/detalhe/'.$prod['id']).'">'.$prod['nome'].'</p></td>';
            $html .= '<td>'.$prod['saber'].'</td>';
            $html .= '<td>'.$prod['preco'].'</td>';
            $html .= '<td>'.$this->action_buttons($prod['id']).'</td>';
            $html .= '</tr>';   
        }
        return $html;
    }

    public function geracar(){
        $html ='';

        $produtos = new Prod();
        $data = $produtos->listacar();

        foreach ($data as $prod) {

            

          if($this->session->userdata('id')==($prod['idcara'])||$this->session->userdata('level')==('1')){  

            $html .= '<tr>';
            $html .= '<td>'.$prod['id'].'</td>';
            $html .= '<td>'.$prod['idcara'].'</td>';
            $html .= '<td>'.$prod['nomecara'].'</td>';
            $html .= '<td>'.$prod['emailcara'].'</td>';
            $html .= '<td>'.$prod['nome'].'</td>';
            $html .= '<td>'.$prod['preco'].'</td>';
            $html .= '<td>'.$this->action_buttons_car($prod['id']).'</td>';
            $html .= '</tr>';   
          }
      
        }
        return $html;
    }
    
    public function gerapr(){
        $html ='';

        $produtos = new Prod();
        $data = $produtos->lista();

        foreach ($data as $prod) {
          
            $html .= '<div class="col-md-3 card mt-3">';
             $html .= '<div class="text-center"><img src="';
            $html .= base_url('uploads/');
            $html .= $prod['imagem'];
            $html .= '.jpg" class="rounded" alt="Imagem do Produto"></div>';
            $html .= '<div class=" text-center">';
           
            $html .= '<div class="text">
            <h3>';
            $html .= '<strong class="text-danger">'.$prod['nome'].'</strong></a></h3>';
            $html .= '<td><p>'.$prod['saber'].'</p></td>';
            
            $html .= '<span class="price mb-4">R$'.$prod['preco'].'</span>';
            $html .= '    
                </div>';
                $html .= '<a href="';
                $html .= base_url('index.php/brands/ver_produtin/');
                $html .= $prod['id'];
                $html .='"><button type="button" class="btn mb-3 btn-success">Ver Mais</button></div></div></a>';
        
        
            }
        return $html;
    }
    


    public function deleta_prod($id){
        $produto= new Prod();
        $produto->deleta($id);
    }
    
    public function deleta_car($id){
        $produto= new Prod();
        $produto->deletacar($id);
    }
    
    
    public function read($id){
        $servicos = new Prod();
        return $servicos->fazobarato($id);
    }
}
