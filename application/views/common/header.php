<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Company Bag</title>
  <link rel="shortcut icon" href="<?= base_url('assets/img/logo.jfif'); ?>" type="image/x-icon">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/LineIcons.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/animate.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/aos.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/slick.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/default.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">
</head>
<body>