<div class="container mb-3">
    <div class="row pt-5">
        <div class="card mx-auto col-md-3">
            <img class="card-img-top" src="<?= base_url('assets/img/000001.jfif'); ?>" alt="Card image cap">

            <div class="card-body">
                <h4 class="card-title"><a>Produto X</a></h4>
                <button type='button' class='main-btn mb-3' data-toggle='modal' data-target='#exampleModal'>
                    Ver Mais
                </button>
            </div>


                <div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                    <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalLabel'>Descrição do Produto:<br/>
                        <br/>
                            
                        </h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                        </div>
                        <div class='modal-body'>
                        Cor, preço, tamanho, material</div>
                        <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary'>Em manutenção</button>
                        </div>
                    </div>
                    </div>
                </div>
            
        </div>

        <div class="card mx-auto col-md-3">
            <img class="card-img-top" src="<?= base_url('assets/img/000002.jfif'); ?>" alt="Card image cap">

            <div class="card-body">
                <h4 class="card-title"><a>Produto X</a></h4>
                <button type='button' class='main-btn mb-3' data-toggle='modal' data-target='#exampleModal'>
                    Ver Mais
                </button>
            </div>
        </div>

        <div class="card mx-auto col-md-3">
            <img class="card-img-top" src="<?= base_url('assets/img/000001.jfif'); ?>" alt="Card image cap">

            <div class="card-body">
                <h4 class="card-title"><a>Produto X</a></h4>
                <button type='button' class='main-btn mb-3' data-toggle='modal' data-target='#exampleModal'>
                    Ver Mais
                </button>
            </div>
        </div>

        <div class="card mx-auto col-md-3">
            <img class="card-img-top" src="<?= base_url('assets/img/000002.jfif'); ?>" alt="Card image cap">

            <div class="card-body">
                <h4 class="card-title"><a>Produto X</a></h4>
                <button type='button' class='main-btn mb-3' data-toggle='modal' data-target='#exampleModal'>
                    Ver Mais
                </button>
            </div>
        </div>
    </div>
</div>