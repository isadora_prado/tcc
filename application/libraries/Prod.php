<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Prod extends CI_Object {
    
    public function insere_p($cas){
        $this->db->insert('produtos', $cas);
    }

    public function lista(){
        $rs = $this->db->get_where('produtos');
        $result = $rs->result_array();
        return $result;
    }

    public function listacar(){
        $rs = $this->db->get_where('carrin');
        $result = $rs->result_array();
        return $result;
    }
    
    public function deleta($id){
        $cond = array('id' => $id);
        return $this->db->delete('produtos', $cond); 
    }

    public function deletacar($id){
        $cond = array('id' => $id);
        return $this->db->delete('carrin', $cond); 
    }
    
   
    public function fazobarato($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('produtos', $cond);
        return $rs->row_array();
    }


}