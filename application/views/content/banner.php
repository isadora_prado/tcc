<div class="header-banner d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-9 col-sm-10">
                        <div class="banner-content">
                            <h4 class="sub-title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1s">Company Bag</h4>
                            <h1 class="banner-title mt-10 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="2s"><span>Bem Vindo!</span> <br/> As melhores bolsas e calçados para você.</h1>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="mt-5 banner-image bg_cover" style="background-image: url(<?= base_url('assets/img/logo.jfif'); ?>); width: 500px; height: 500px"></div>
        </div>