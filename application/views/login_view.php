<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sign In</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
  </head>
  <body>

      <div class="container mt-5 pt-5">
       <div class="col-md-4 mt-5 pt-5 col-md-offset-4">
         <form class="form-signin" action="<?php echo base_url('index.php/login/auth');?>" method="post">
           <h2 class="form-signin-heading text-center">LOGIN</h2>
           <?php echo $this->session->flashdata('msg');?>
           <label for="username" class="sr-only">E-mail</label>
           <input type="email" name="email" class="form-control mb-2" placeholder="Email" required autofocus>
           <label for="password" class="sr-only">Senha</label>
           <input type="password" name="password" class="form-control" placeholder="Senha" required>
           <div class="mt-3 mb-2">
            <a href="<?= base_url('index.php/login/cadastrar') ?>">Criar conta</a>
           </div>
           <button class="btn col-3 main-btn btn-block mx-auto" type="submit">Entrar</button>
         </form>
       </div>
       </div> 
       <br><br><br>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
  </body>
</html>
